package com.nexus6.post.bus.service;

import com.nexus6.post.model.Client;
import com.nexus6.post.model.dao.DAO;
import com.nexus6.post.model.dao.ClientDao;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ClientService implements Fetchable<Client> {
  
  public List<Client> findAll() throws SQLException {
    DAO<Client, Integer> data = new ClientDao();
    List<Client> output = data.getAll();
    data.closeConnection();
    return output;
  }

  public boolean addClient(Client c) throws SQLException {
    DAO<Client, Integer> data = new ClientDao();
    boolean output = data.create(c);
    data.closeConnection();
    return output;
  }

  public Client fetchClientById(Integer id) throws SQLException {
    DAO<Client, Integer> data = new ClientDao();
    Optional<Client> output = data.getById(id);
    data.closeConnection();
    return output.orElseThrow(IllegalArgumentException::new);
  }

  public Optional<Client> fetchClientById(String phone) throws SQLException {
    ClientDao data = new ClientDao();
    Optional<Client> output = data.getById(phone);
    data.closeConnection();
    return output;
  }

  public boolean changeClient(Client c) throws SQLException {
    DAO<Client, Integer> data = new ClientDao();
    boolean output = data.update(c);
    data.closeConnection();
    return output;
  }

  public boolean removeClient(Integer id) throws SQLException {
    DAO<Client, Integer> data = new ClientDao();
    boolean output = data.delete(id);
    data.closeConnection();
    return output;
  }
}
