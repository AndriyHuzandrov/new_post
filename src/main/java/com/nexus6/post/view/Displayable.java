package com.nexus6.post.view;

public interface Displayable {
  void display(String data);
}
