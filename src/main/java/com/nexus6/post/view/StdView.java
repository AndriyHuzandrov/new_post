package com.nexus6.post.view;

import static com.nexus6.post.utils.TxtConsts.*;

public class StdView implements Displayable {

  public void display(String data) {
    appLog.info(data);
  }
}
