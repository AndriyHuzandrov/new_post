package com.nexus6.post.UI;

import com.nexus6.post.menu.ClientLoginMenu;
import com.nexus6.post.menu.MainMenu;
import com.nexus6.post.menu.MaintainMenu;
import com.nexus6.post.menu.Menu;
import com.nexus6.post.menu.ParcelProcessMenu;
import com.nexus6.post.menu.ParcelRedirectMenu;
import com.nexus6.post.menu.TrackParcelMenu;
import com.nexus6.post.menu.routeene.ClientLoginTxt;
import com.nexus6.post.menu.routeene.MainTxt;
import com.nexus6.post.menu.routeene.MaintainTxt;
import com.nexus6.post.menu.routeene.ParcelProcessTxt;
import com.nexus6.post.menu.routeene.Showable;
import com.nexus6.post.menu.routeene.TrackTxt;

public class UI {

  public static Menu newMainMenu() {
    Menu m = new MainMenu();
    m.setTxtMenu(new MainTxt());
    return m;
  }

  public static Menu newParcTrackMenu(String phone) {
    Menu m = new TrackParcelMenu();
    m.setTxtMenu(new TrackTxt());
    m.setClient_id(phone);
    return m;
  }

  public static Menu newClientLoginMenu() {
    Menu m = new ClientLoginMenu();
    m.setTxtMenu(new ClientLoginTxt());
    return m;
  }

  public static Menu newRedirectMenu(Showable txtMenu, String client_owner, String parc_id) {
    Menu m = new ParcelRedirectMenu();
    m.setClient_id(client_owner);
    m.setParcel_id(parc_id);
    m.setTxtMenu(txtMenu);
    return m;
  }

  public static Menu newMaintainMenu() {
    Menu m = new MaintainMenu();
    m.setTxtMenu(new MaintainTxt());
    return m;
  }

  public static Menu newParcelProcesMenu() {
    Menu m = new ParcelProcessMenu();
    m.setTxtMenu(new ParcelProcessTxt());
    return m;
  }
}
