package com.nexus6.post.menu;

import com.nexus6.post.UI.UI;
import com.nexus6.post.model.Parcel;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class ParcelProcessMenu extends Menu
{
  HashMap<String, Performable> prepareExeMenu() {
    HashMap<String, Performable> menu = new LinkedHashMap<>();
    menu.put("0", this::exit);
    menu.put("1", this::sendParcel);
    menu.put("2", this::receiveParcel);
    return menu;
  }
  private void sendParcel() {
    Parcel currParcell = appControl.checkInParcel();
  }

  private void receiveParcel() {}

  void exit() {
    Menu m = UI.newMainMenu();
    m.getChoice();
  }

}
