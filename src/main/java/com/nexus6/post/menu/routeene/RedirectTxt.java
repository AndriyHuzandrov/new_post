package com.nexus6.post.menu.routeene;

import static com.nexus6.post.utils.TxtConsts.LEAVE;

import java.util.LinkedList;
import java.util.List;

public class RedirectTxt extends Showable {

  List<String> fillMenu() {
    menu = new LinkedList<>();
    menu.add("1. Redirect this parcel.");
    menu.add(LEAVE);
    return menu;
  }

}
