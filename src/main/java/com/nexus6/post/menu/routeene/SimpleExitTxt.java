package com.nexus6.post.menu.routeene;

import static com.nexus6.post.utils.TxtConsts.LEAVE;

import java.util.LinkedList;
import java.util.List;

public class SimpleExitTxt extends Showable {

  List<String> fillMenu() {
    menu = new LinkedList<>();
    menu.add(LEAVE);
    return menu;
  }
}
