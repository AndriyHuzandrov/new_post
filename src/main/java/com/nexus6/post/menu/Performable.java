package com.nexus6.post.menu;

@FunctionalInterface
public interface Performable {
  void perform();
}
