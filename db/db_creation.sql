-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema new_post_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema new_post_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `new_post_db` DEFAULT CHARACTER SET utf8 ;
USE `new_post_db` ;

-- -----------------------------------------------------
-- Table `new_post_db`.`district`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`district` (
  `id_district` INT NOT NULL AUTO_INCREMENT,
  `distr_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_district`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`city` (
  `id_city` INT NOT NULL AUTO_INCREMENT,
  `city_name` VARCHAR(45) NOT NULL,
  `district` INT NOT NULL,
  PRIMARY KEY (`id_city`),
  INDEX `fk_city_district_idx` (`district` ASC),
  CONSTRAINT `fk_city_district`
    FOREIGN KEY (`district`)
    REFERENCES `new_post_db`.`district` (`id_district`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`client` (
  `id_client` INT NOT NULL AUTO_INCREMENT,
  `cl_name` VARCHAR(15) NOT NULL,
  `cl_surname` VARCHAR(20) NOT NULL,
  `mobile` VARCHAR(9) NOT NULL,
  `passport` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id_client`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`department` (
  `id_dep` INT NOT NULL,
  `dep_nr` SMALLINT(3) NOT NULL,
  `city` INT NOT NULL,
  `adress` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_dep`),
  INDEX `fk_department_city1_idx` (`city` ASC),
  UNIQUE INDEX `city_UNIQUE` (`city` ASC),
  UNIQUE INDEX `adress_UNIQUE` (`adress` ASC),
  CONSTRAINT `fk_department_city1`
    FOREIGN KEY (`city`)
    REFERENCES `new_post_db`.`city` (`id_city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`price`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`price` (
  `cat` TINYINT(2) NOT NULL,
  `price` DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (`cat`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`parcell`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`parcell` (
  `id_parcell` INT NOT NULL,
  `weight` DECIMAL(5,1) NOT NULL,
  `vol` DECIMAL(3,1) NOT NULL,
  `send_date` DATETIME NOT NULL,
  `recieve_date` DATETIME NULL,
  `sender` INT NOT NULL,
  `reciever` INT NOT NULL,
  `transp_cost` DECIMAL(6,2) NOT NULL,
  `price_cat` TINYINT(2) NOT NULL DEFAULT 1,
  `from_dep` INT NOT NULL,
  `descr` BLOB NULL,
  PRIMARY KEY (`id_parcell`),
  INDEX `fk_parcell_client1_idx` (`sender` ASC, `reciever` ASC),
  INDEX `fk_parcell_price1_idx` (`price_cat` ASC),
  INDEX `fk_parcell_department1_idx` (`from_dep` ASC),
  CONSTRAINT `fk_parcell_client1`
    FOREIGN KEY (`sender` , `reciever`)
    REFERENCES `new_post_db`.`client` (`id_client` , `id_client`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcell_price1`
    FOREIGN KEY (`price_cat`)
    REFERENCES `new_post_db`.`price` (`cat`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcell_department1`
    FOREIGN KEY (`from_dep`)
    REFERENCES `new_post_db`.`department` (`id_dep`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`distance`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`distance` (
  `route` INT NOT NULL AUTO_INCREMENT,
  `from_city` INT NOT NULL,
  `to_city` INT NOT NULL,
  `dist` SMALLINT(4) NOT NULL,
  INDEX `fk_distance_city1_idx` (`from_city` ASC, `to_city` ASC),
  PRIMARY KEY (`route`),
  CONSTRAINT `fk_distance_city1`
    FOREIGN KEY (`from_city` , `to_city`)
    REFERENCES `new_post_db`.`city` (`id_city` , `id_city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`transit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`transit` (
  `parcell` INT NOT NULL,
  `to_dep` INT NOT NULL,
  `route` INT NOT NULL,
  `carrier_id` INT NOT NULL,
  INDEX `fk_parcell_has_department_department1_idx` (`to_dep` ASC),
  INDEX `fk_parcell_has_department_parcell1_idx` (`parcell` ASC),
  UNIQUE INDEX `parcell_UNIQUE` (`parcell` ASC),
  UNIQUE INDEX `to_dep_UNIQUE` (`to_dep` ASC),
  INDEX `fk_transit_distance1_idx` (`route` ASC),
  CONSTRAINT `fk_parcell_has_department_parcell1`
    FOREIGN KEY (`parcell`)
    REFERENCES `new_post_db`.`parcell` (`id_parcell`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_parcell_has_department_department1`
    FOREIGN KEY (`to_dep`)
    REFERENCES `new_post_db`.`department` (`id_dep`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_transit_distance1`
    FOREIGN KEY (`route`)
    REFERENCES `new_post_db`.`distance` (`route`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `new_post_db`.`store`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `new_post_db`.`store` (
  `parcell` INT NOT NULL,
  `dep` INT NOT NULL,
  `status` CHAR(1) NOT NULL,
  INDEX `fk_store_parcell1_idx` (`parcell` ASC),
  INDEX `fk_store_department1_idx` (`dep` ASC),
  CONSTRAINT `fk_store_parcell1`
    FOREIGN KEY (`parcell`)
    REFERENCES `new_post_db`.`parcell` (`id_parcell`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_store_department1`
    FOREIGN KEY (`dep`)
    REFERENCES `new_post_db`.`department` (`id_dep`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
